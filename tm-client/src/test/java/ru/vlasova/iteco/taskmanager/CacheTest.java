package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Test;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;

public class CacheTest extends AbstractTest {

    @After
    public void after() throws Exception {
        taskEndpoint.removeAllTasksByUserId(tokenUser, userId);
        taskEndpoint.removeAllTasksByUserId(tokenAdmin, adminId);
        projectEndpoint.removeAllProjectByUserId(tokenUser, userId);
        projectEndpoint.removeAllProjectByUserId(tokenAdmin, adminId);
    }

    @Test
    public void  findOneProject() throws Exception {
        @NotNull final ProjectDTO project = createProject(userId);
        projectEndpoint.persistProject(tokenUser, project);
        for(int i = 0; i < 15; i++) {
            projectEndpoint.findOneProjectByUserId(tokenUser, userId, project.getId());
        }
    }

    private ProjectDTO createProject(String userId) throws Exception {
        @NotNull final ProjectDTO project = projectEndpoint.insertProject(tokenUser, userId, "TestProject",
                "Description 123", "10.01.2002", "15.10.2005");
        return project;
    }
}
