package ru.vlasova.iteco.taskmanager.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.List;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    protected String token;

    @Getter
    protected String userId;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract boolean secure();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public List<Role> getRole() {
        return null;
    }

    public void printProjectList(@Nullable final String userId) throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final List<ProjectDTO> projectList = projectEndpoint.findAllProjectsByUserId(token, userId);
        if (projectList == null || projectList.size() == 0) {
            terminalService.print("There are no projects.");
            return;
        }
        int i = 1;
        for (@NotNull ProjectDTO project : projectList) {
            terminalService.print(i++ + ": " + project.getName());
        }
    }

    public void printTaskList(@Nullable final List<TaskDTO> taskList) {
        int i = 1;
        if (taskList == null) return;
        for (@Nullable TaskDTO task : taskList) {
            serviceLocator.getTerminalService().print(i++ + ": " + task.getName());
        }
    }

    public void validSession() throws Exception {
        token = serviceLocator.getStateService().getToken();
        if(token == null) {
            serviceLocator.getTerminalService().print("Access denied");
            return;
        }
        userId = serviceLocator.getSessionEndpoint().getCurrentUserId(token);
        @Nullable final List<Role> roles = getRole();
        @NotNull final boolean checkRole = roles == null ||
                serviceLocator.getUserEndpoint().checkRole(userId, roles);
        if(!checkRole) throw new Exception("Access denied!");
    }

}