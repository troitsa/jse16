package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "project_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        printProjectList(userId);
        terminalService.print("To delete project choose the project and type the number");
        @Nullable final String indexString = terminalService.readString();
        if(indexString == null || indexString.trim().length() == 0) {
            terminalService.print("No such project.");
            return;
        }
        final int index = Integer.parseInt(indexString)-1;
        if(index < 0) return;
        @Nullable final ProjectDTO project = projectEndpoint.getProjectByIndex(token, userId, index);
        projectEndpoint.removeProjectById(token, project.getId());
        terminalService.print("Project delete.");
    }

}