package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.service.StateService;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public interface ServiceLocator {

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    TerminalService getTerminalService();

    @NotNull
    StateService getStateService();

}
