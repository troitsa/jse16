package ru.vlasova.iteco.taskmanager.error;

import org.jetbrains.annotations.NotNull;

public final class AccessDeniedException extends Exception {

    public AccessDeniedException(@NotNull final String message) {
        super(message);
    }

}

