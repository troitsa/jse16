package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Getter
@Setter
public class EntityManagerService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    public void init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("taskmanager");
    }

    public EntityManagerService(@NotNull final ServiceLocator serviceLocator)  {
        this.serviceLocator = serviceLocator;
    }

    public EntityManager getEntityManager()  {
        return entityManagerFactory.createEntityManager();
    }

}

