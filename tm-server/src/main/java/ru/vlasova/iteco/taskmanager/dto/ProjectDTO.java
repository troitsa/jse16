package ru.vlasova.iteco.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends ItemDTO {

    public ProjectDTO(@NotNull String userId) {
        this.userId = userId;
    }

}
