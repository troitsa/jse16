package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    List<Task> getTasksByProjectId(@NotNull final String userId,
                                   @NotNull final String projectId);

    @NotNull
    List<Task> search(@NotNull final String userId,
                      @NotNull final String searchString);

    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    Task findOneByUserId(@NotNull final String userId,
                         @NotNull final String id);

    void removeAllByUserId(@NotNull final String userId);

    void removeByUserId(@NotNull final String userId,
                        @NotNull final String id);

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findOne(@NotNull final String id);

    void persist(@NotNull final Task task);

    void merge(@NotNull final Task task);

    void remove(@NotNull final String id);

    void removeAll();

    @NotNull
    List<Task> sortTask(@Nullable final String userId,
                        @Nullable final String sortMode);

}