package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public TaskService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.getTasksByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final User user, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || user == null) return null;
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) {
        if (userId == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = getTaskByIndex(userId, index);
        if (task == null) return;
        repository.removeByUserId(userId, task.getId());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeByUserId(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0 ) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList.get(index);
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) {
        if (userId == null || searchString == null || searchString.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.search(userId, searchString);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOneByUserId(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @Nullable List<Task> sortTask(@Nullable final String userId,
                                         @Nullable final String sortMode) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> taskList = repository.sortTask(userId, sortMode);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskList;
    }
}
