package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    void removeSession(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    SessionDTO findOneSession(@Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    String getToken(@Nullable final String login, @Nullable String password) throws Exception;

    @Nullable
    @WebMethod
    String getCurrentUserId(@Nullable final String token) throws Exception;

}
